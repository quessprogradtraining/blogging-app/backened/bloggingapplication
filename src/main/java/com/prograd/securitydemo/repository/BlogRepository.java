package com.prograd.securitydemo.repository;

import com.prograd.securitydemo.model.Blogger;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface BlogRepository extends JpaRepository<Blogger,Integer> {
}
