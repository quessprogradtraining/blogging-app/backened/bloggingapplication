package com.prograd.securitydemo.controller;

import com.prograd.securitydemo.service.BlogService;
import com.prograd.securitydemo.model.Blogger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class BlogController {
    @Autowired
    BlogService blogService;


    @PostMapping("/addblog")
    public String addBlog(@RequestBody Blogger bloggerObject){
        String stringObject=blogService.addBlog(bloggerObject);
        return stringObject;
    }

    @PutMapping("/updateblog/{blogId}")
    public void updateBlog(@RequestBody Blogger bloggerobject, @PathVariable int blogId){
        blogService.updateBlog(bloggerobject,blogId);
    }

    @DeleteMapping("/deleteBlog/{blogId}")
    public void deleteBlog(@PathVariable int blogId){
       blogService.deleteBlog(blogId);
    }

    @GetMapping("/displayBlog")
    public List<Blogger> displayBlog(){
        return blogService.displayBlog();
    }

}
