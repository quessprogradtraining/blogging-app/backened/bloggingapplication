package com.prograd.securitydemo.service;

import com.prograd.securitydemo.model.Blogger;
import com.prograd.securitydemo.repository.BlogRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
@Service
public class BlogService {
    @Autowired
    BlogRepository blogRepository;
    public String addBlog(Blogger bloggerObject) {
        blogRepository.save(bloggerObject);
        return "Blog is added successfully";
    }

    public void updateBlog(Blogger bloggerobject, int blogId) {
        Blogger fetchedBlog=blogRepository.findById(blogId).get();
        if(fetchedBlog!=null){
            blogRepository.delete(fetchedBlog);
           blogRepository.save(bloggerobject);

        }

    }

    public void deleteBlog(int blogId) {
        blogRepository.deleteById(blogId);
    }

    public List<Blogger> displayBlog() {
        return blogRepository.findAll();
    }
}
